package com.startup.movieku.view.review

import android.content.Context
import com.example.themoviedb.data.model.ResultXX
import com.startup.movieku.R
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_ulasan.view.*

class ReviewRecyclerViewItem(val resul: ResultXX, context: Context) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        val author = viewHolder.itemView.author
        val conten = viewHolder.itemView.conten

        author.text = resul.author
        conten.text = resul.content
    }

    override fun getLayout(): Int = R.layout.item_ulasan

}
package com.startup.movieku.view.listmovie

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.themoviedb.data.model.MovieList
import com.startup.movieku.repository.MovieRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MovieVM(private val repo: MovieRepo) : ViewModel() {
    val movieData = MutableLiveData<MovieList>()

    suspend fun getMoviestList(page: Int, genre: Int) = withContext(Dispatchers.IO) {
        if (repo.getMovie(page, genre)?.results.isNullOrEmpty()) {
            movieData.postValue(null)
        } else {
            movieData.postValue(repo.getMovie(page, genre))
        }

    }

}
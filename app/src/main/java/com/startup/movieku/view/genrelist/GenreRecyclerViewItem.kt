package com.startup.movieku.view.genrelist

import android.content.Context
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import com.example.themoviedb.data.model.Genre
import com.startup.movieku.R
import com.startup.movieku.utilities.RandomBg
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_list_genre.view.*

class GenreRecyclerViewItem(val genre: Genre, val context: Context) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        val genrename = viewHolder.itemView.genre
        val bg = viewHolder.itemView.bg

        genrename.text = genre.name
        bg.setBackgroundColor(RandomBg.getRandomColor())

        viewHolder.itemView.setOnClickListener {
            val bundle = bundleOf("id" to genre.id.toString())
            it.findNavController().navigate(R.id.action_genreFragment_to_movieFragment, bundle)
        }
    }

    override fun getLayout(): Int = R.layout.item_list_genre
}


package com.startup.movieku.view.review

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.themoviedb.data.model.ReviewsList
import com.startup.movieku.repository.ReviewRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ReviewVM(private val repo: ReviewRepo) : ViewModel() {
    val reviewData = MutableLiveData<ReviewsList>()

    suspend fun getReview(movieId: Int, page: Int) = withContext(Dispatchers.IO) {
        if (repo.getReview(movieId, page)?.results.isNullOrEmpty()) {
            reviewData.postValue(null)
        } else {
            reviewData.postValue(repo.getReview(movieId, page))
        }
    }
}
package com.startup.movieku.view.review

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.themoviedb.data.model.ResultXX
import com.startup.movieku.R
import com.startup.movieku.utilities.CheckingNetwork
import com.startup.movieku.utilities.PaginationScrollListener
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_review.*
import kotlinx.android.synthetic.main.item_ulasan.*
import kotlinx.android.synthetic.main.loading_screen.*
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


class ReviewFragment : Fragment() {

    private val vm: ReviewVM by inject()
    private val adapters = GroupAdapter<ViewHolder>()
    val data: MutableList<ResultXX> = mutableListOf()
    private var isLoadMore = false
    private var isLastPage = false
    var page = 1

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_review, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapters.clear()

        val id = arguments?.getInt("id", 0)

        if (CheckingNetwork.isOnline(requireContext())) {
            viewLifecycleOwner.lifecycleScope.launch {
                getVM(id!!, page)
            }
        } else {
            CheckingNetwork.checkinternettoast(requireContext())
        }

        val layoutManagers = LinearLayoutManager(context)
        review.apply {
            layoutManager = layoutManagers
            adapter = adapters
        }

        review.addOnScrollListener(object : PaginationScrollListener(layoutManagers) {
            override fun isLastPage(): Boolean = isLastPage

            override fun isLoading(): Boolean = isLoadMore

            override fun loadMoreItems() {
                isLoadMore = true
                page++
                itemloading1.visibility = View.VISIBLE
                viewLifecycleOwner.lifecycleScope.launch {
                    getVM(id!!, page)
                }
            }

        })
    }

    suspend fun getVM(movieId: Int, page: Int) {
        vm.getReview(movieId, page)
        vm.reviewData.observe(viewLifecycleOwner, { ulasan ->
            loading.visibility = View.GONE

            if (page == 1 && ulasan == null) {
                isEmpty.visibility = View.VISIBLE
            } else {
                isEmpty.visibility = View.GONE
            }

            if (ulasan?.results.isNullOrEmpty()) {
                isLastPage = true
                itemloading1.visibility = View.GONE
            } else {
                adapters.clear()
                if (isLoadMore) {
                    itemloading1.visibility = View.GONE
                    isLoadMore = false
                }

                data.addAll(ulasan.results)
                data.map {
                    adapters.add(ReviewRecyclerViewItem(it, requireContext()))
                }
            }

        })
    }
}
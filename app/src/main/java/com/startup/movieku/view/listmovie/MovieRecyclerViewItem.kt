package com.startup.movieku.view.listmovie

import android.content.Context
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import com.example.themoviedb.data.model.Result
import com.squareup.picasso.Picasso
import com.startup.movieku.R
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_movielist.view.*

class MovieRecyclerViewItem(private val result: Result, context: Context) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        val poster = viewHolder.itemView.poster
        val title = viewHolder.itemView.title
        val rating = viewHolder.itemView.rating

        Picasso.get().load("https://image.tmdb.org/t/p/w185//" + result.poster_path)
            .error(R.drawable.ic_launcher_background)
            .into(poster)
        title.text = result.title
        rating.rating = (result.vote_average - 4.0).toFloat()

        viewHolder.itemView.setOnClickListener {
            val bundle = bundleOf("id" to result.id)
            it.findNavController()
                .navigate(R.id.action_movieFragment_to_detailMovieFragment, bundle)
        }
    }

    override fun getLayout(): Int = R.layout.item_movielist
}
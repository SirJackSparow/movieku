package com.startup.movieku.view.genrelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.startup.movieku.R
import com.startup.movieku.utilities.CheckingNetwork
import com.startup.movieku.utilities.Token
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_genre.*
import kotlinx.android.synthetic.main.loading_screen.*
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject

class GenreFragment : Fragment() {
    private val adapterGenre = GroupAdapter<ViewHolder>()
    private val vm: GenreVM by inject()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_genre, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapterGenre.clear()
        if (CheckingNetwork.isOnline(this.requireContext())) {
            viewLifecycleOwner.lifecycleScope.launch {
                getGenreVM()
            }
        } else {
            CheckingNetwork.checkinternettoast(requireContext())
        }
        genres.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = adapterGenre
        }

    }

    suspend fun getGenreVM() {
        vm.getGenre(Token.api_key, Token.laguage)
        vm.dataGenre.observe(viewLifecycleOwner, {

            loading.visibility = View.GONE
            it.genres.map { genre ->
                adapterGenre.add(GenreRecyclerViewItem(genre, this.requireContext()))

            }


        })
    }
}
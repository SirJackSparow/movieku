package com.startup.movieku.view.genrelist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.themoviedb.data.model.GenreList
import com.startup.movieku.repository.GenreRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class GenreVM(val repo: GenreRepo) :  ViewModel() {

    val dataGenre = MutableLiveData<GenreList>()

    suspend fun getGenre(api_key: String, language: String) = withContext(Dispatchers.IO) {
        dataGenre.postValue(repo.getGenre(api_key, language))
    }
}
package com.startup.movieku.view.moviedetail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import com.startup.movieku.R
import com.startup.movieku.utilities.CheckingNetwork
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_detail_movie.*
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


class DetailMovieFragment : Fragment() {

    private val vm: DetailMovieVM by inject()
    private val videoAdapter = GroupAdapter<ViewHolder>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail_movie, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        videoAdapter.clear()
        val id = arguments?.getInt("id", 0)
        if (CheckingNetwork.isOnline(requireContext())) {
            viewLifecycleOwner.lifecycleScope.launch {
                getData(id!!)
            }
        } else {
            CheckingNetwork.checkinternettoast(requireContext())
        }

        videoTriller.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL,false)
            adapter = videoAdapter
        }

        ulasanBtn.setOnClickListener {
            val bundle = bundleOf("id" to id)
            it.findNavController().navigate(
                R.id.action_detailMovieFragment_to_reviewFragment
                , bundle
            )
        }
    }

    suspend fun getData(movieId: Int) {
        vm.getDetailMovie(movieId)
        vm.detailMovie.observe(viewLifecycleOwner, {
            titleMovie.text = it.title
            detailRating.rating = (it.vote_average - 5.0).toFloat()
            overviewContent.text = it.overview
            Picasso.get().load("https://image.tmdb.org/t/p/w185//" + it.backdrop_path)
                .error(R.drawable.ic_launcher_background)
                .into(poster1)
        })

        vm.getVideo(movieId)
        vm.movieData.observe(viewLifecycleOwner, { triller ->

            triller.results.map { trailer ->
                videoAdapter.add(TrillerRecyclerViewItem(trailer, requireContext()))
            }
        })
    }


}
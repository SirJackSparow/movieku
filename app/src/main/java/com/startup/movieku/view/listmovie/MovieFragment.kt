package com.startup.movieku.view.listmovie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.example.themoviedb.data.model.Result
import com.startup.movieku.R
import com.startup.movieku.utilities.CheckingNetwork
import com.startup.movieku.utilities.PaginationScrollListener
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.fragment_movie.*
import kotlinx.android.synthetic.main.loading_screen.*
import kotlinx.coroutines.launch
import org.koin.android.ext.android.inject


class MovieFragment : Fragment() {

    private var isLoadMore = false
    private var isLastPage = false
    var page = 1

    var data = mutableListOf<Result>()
    private val vm: MovieVM by inject()
    val adapterMovie = GroupAdapter<ViewHolder>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movie, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapterMovie.clear()
        data.clear()

        val id = arguments?.getString("id")

        if (CheckingNetwork.isOnline(requireContext())) {
            viewLifecycleOwner.lifecycleScope.launch {
                getMovie(page, id!!.toInt())
            }
        } else {
            CheckingNetwork.checkinternettoast(requireContext())
        }

        val gridLayoutManager = GridLayoutManager(context, 2)
        movie.apply {
            layoutManager = gridLayoutManager
            adapter = adapterMovie
        }

        movie.addOnScrollListener(object : PaginationScrollListener(gridLayoutManager) {
            override fun isLastPage(): Boolean = isLastPage

            override fun isLoading(): Boolean = isLoadMore

            override fun loadMoreItems() {
                isLoadMore = true
                page++
                itemloading.visibility = View.VISIBLE
                viewLifecycleOwner.lifecycleScope.launch {
                    getMovie(page, id!!.toInt())
                }
            }

        })
    }


    suspend fun getMovie(page: Int, genre: Int) {
        vm.getMoviestList(page, genre)
        vm.movieData.observe(viewLifecycleOwner, { movie ->
            loading.visibility = View.GONE

            if (movie?.results.isNullOrEmpty()) {
                isLastPage = true
                itemloading.visibility = View.GONE
            } else {
                adapterMovie.clear()
                if (isLoadMore) {
                    itemloading.visibility = View.GONE
                    isLoadMore = false
                }

                data.addAll(movie.results)
                data.map {
                    adapterMovie.add(MovieRecyclerViewItem(it, requireContext()))
                }

            }

        })
    }
}
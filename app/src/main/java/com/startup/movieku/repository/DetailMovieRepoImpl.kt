package com.startup.movieku.repository

import com.example.themoviedb.data.model.TraillerList
import com.example.themoviedb.data.model.detailmovie.DetailMovie
import com.startup.movieku.data.network.NetworkService
import com.startup.movieku.utilities.Token

class DetailMovieRepoImpl(
    private val network : NetworkService
) :  DetailMovieRepo {
    override suspend fun getDetail(movie_id: Int): DetailMovie? {
        return if (network.detailMovie(movie_id,Token.api_key, Token.laguage).isSuccessful) {
            network.detailMovie(movie_id,Token.api_key, Token.laguage).body()
        } else {
            null
        }
    }

    override suspend fun getTrailer(movie_id: Int): TraillerList? {
        return if (network.getTraillerMovie(
                movie_id,
                Token.api_key,
                Token.laguage
            ).isSuccessful
        ) {
            network.getTraillerMovie( movie_id,Token.api_key, Token.laguage).body()
        } else {
            null
        }
    }
}
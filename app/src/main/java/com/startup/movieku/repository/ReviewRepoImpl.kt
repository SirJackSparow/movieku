package com.startup.movieku.repository

import com.example.themoviedb.data.model.ReviewsList
import com.startup.movieku.data.network.NetworkService
import com.startup.movieku.utilities.Token

class ReviewRepoImpl(
    private val networkService: NetworkService
) : ReviewRepo {
    override suspend fun getReview(movie_id: Int, page: Int): ReviewsList? {
        return if (networkService.getReviewUser(
                movie_id,
                Token.api_key,
                Token.laguage,
                page
            ).isSuccessful
        ) {
            networkService.getReviewUser(movie_id, Token.api_key, Token.laguage, page).body()
        } else {
            null
        }
    }

}
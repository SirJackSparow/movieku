package com.startup.movieku.repository

import com.example.themoviedb.data.model.GenreList

interface GenreRepo {
    suspend fun getGenre(api_key: String, language: String): GenreList
}
package com.startup.movieku.repository

import com.example.themoviedb.data.model.ReviewsList

interface ReviewRepo {
    suspend fun getReview(movie_id:Int,page:Int) :  ReviewsList?
}
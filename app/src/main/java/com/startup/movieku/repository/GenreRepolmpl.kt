package com.startup.movieku.repository

import com.example.themoviedb.data.model.GenreList
import com.startup.movieku.data.network.NetworkService

class GenreRepolmpl(
   private val network : NetworkService
) : GenreRepo {
    override suspend fun getGenre(api_key: String, language: String): GenreList {
        return network.getGenreList(api_key, language).body()!!
    }
}
package com.startup.movieku.repository

import com.example.themoviedb.data.model.TraillerList
import com.example.themoviedb.data.model.detailmovie.DetailMovie

interface DetailMovieRepo {

    suspend  fun getDetail(movie_id:Int) : DetailMovie?

    suspend fun getTrailer(movie_id:Int) : TraillerList?
}
package com.startup.movieku.repository

import com.example.themoviedb.data.model.MovieList
import com.startup.movieku.data.network.NetworkService
import com.startup.movieku.utilities.Token

class MovieRepoImpl( private val networkService: NetworkService) :  MovieRepo {
    override suspend fun getMovie(page: Int, genre: Int): MovieList? {
        return if (networkService.getMovieList(
                Token.api_key,
                Token.laguage,
                page,
                genre
            ).isSuccessful
        ) {
            networkService.getMovieList(Token.api_key, Token.laguage, page, genre).body()
        } else {
            null
        }
    }
}
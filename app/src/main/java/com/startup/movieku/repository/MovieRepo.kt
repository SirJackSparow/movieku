package com.startup.movieku.repository

import com.example.themoviedb.data.model.MovieList

interface  MovieRepo {
    suspend fun getMovie(page: Int, genre: Int): MovieList?
}
package com.startup.movieku.koin

import com.google.gson.GsonBuilder
import com.startup.movieku.data.network.NetworkService
import com.startup.movieku.repository.*
import com.startup.movieku.view.genrelist.GenreVM
import com.startup.movieku.view.listmovie.MovieVM
import com.startup.movieku.view.moviedetail.DetailMovieVM
import com.startup.movieku.view.review.ReviewVM
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {
    single { webService<NetworkService>() }
}

val vmModule = module {

    //movie
    single { MovieRepoImpl(get()) as MovieRepo }
    viewModel { MovieVM(get()) }
    //detail
    single { DetailMovieRepoImpl(get()) as DetailMovieRepo }
    viewModel { DetailMovieVM(get()) }
    //genre
    single { GenreRepolmpl(get()) as GenreRepo }
    viewModel { GenreVM(get()) }

    //review
    single { ReviewRepoImpl(get()) as ReviewRepo}
    viewModel { ReviewVM(get()) }


}


inline fun <reified T> webService(): T {
    val gson = GsonBuilder()
        .setDateFormat("yyyy-MM-dd HH:mm:ss")
        .create()
    val retrofit = Retrofit.Builder()
        .baseUrl("https://api.themoviedb.org/3/")
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
    return retrofit.create(T::class.java)
}

val myAppModule = listOf(networkModule, vmModule)
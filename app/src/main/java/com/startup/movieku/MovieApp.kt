package com.startup.movieku

import android.app.Application
import com.startup.movieku.koin.myAppModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MovieApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MovieApp)
            modules(myAppModule)
        }
    }
}
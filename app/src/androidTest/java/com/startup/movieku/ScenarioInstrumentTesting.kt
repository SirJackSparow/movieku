package com.startup.movieku

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.startup.movieku.view.genrelist.GenreFragment
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito


@RunWith(AndroidJUnit4::class)
class ScenarioInstrumentTesting {

    val mockNavController = Mockito.mock(NavController::class.java)


    @Test
    fun testNavFragmentPositiveShowRecyclerGenre() {
        val titleScenario = launchFragmentInContainer<GenreFragment>()
        titleScenario.onFragment { fragment ->
            Navigation.setViewNavController(fragment.requireView(), mockNavController)
        }
        onView(withId(R.id.genres))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

}
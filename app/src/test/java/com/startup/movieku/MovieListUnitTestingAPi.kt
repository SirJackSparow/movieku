package com.startup.movieku

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.themoviedb.data.model.MovieList
import com.startup.movieku.koin.myAppModule
import com.startup.movieku.view.listmovie.MovieVM
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MovieListUnitTestingAPi : KoinTest {
  private  val vm: MovieVM by inject()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var observerData: Observer<MovieList>


    @Before
    fun before() {

        MockitoAnnotations.initMocks(this)
        startKoin { modules(myAppModule) }
    }

    @After
    fun after() {
        stopKoin()
    }

    //positive case
    @Test
    fun apiPositive() = runBlocking {

        vm.movieData.observeForever(observerData)
        vm.getMoviestList(1,12)
        Assert.assertNotNull(vm.movieData.value)
    }


    //negative case
    @Test
    fun apiNegative() = runBlocking {
        vm.movieData.observeForever(observerData)
        vm.getMoviestList(1,12000)
        Assert.assertNotNull(vm.movieData.value)
    }
}
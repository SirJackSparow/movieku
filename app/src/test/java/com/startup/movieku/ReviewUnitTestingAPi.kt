package com.startup.movieku

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.themoviedb.data.model.ReviewsList
import com.startup.movieku.koin.myAppModule
import com.startup.movieku.view.review.ReviewVM
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class ReviewUnitTestingAPi : KoinTest {

    private val vm: ReviewVM by inject()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var observerData: Observer<ReviewsList>


    @Before
    fun before() {

        MockitoAnnotations.initMocks(this)
        startKoin { modules(myAppModule) }
    }

    @After
    fun after() {
        stopKoin()
    }

    //positive case
    @Test
    fun apiPositive() = runBlocking {

        vm.reviewData.observeForever(observerData)
        vm.getReview(12, 1)
        Assert.assertNotNull(vm.reviewData.value)
    }


    //negative case
    @Test
    fun apiNegative() = runBlocking {
        vm.reviewData.observeForever(observerData)
        vm.getReview(1541930842, 1)
        Assert.assertNotNull(vm.reviewData.value)
    }
}
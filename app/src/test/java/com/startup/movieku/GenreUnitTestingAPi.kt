package com.startup.movieku

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.themoviedb.data.model.GenreList
import com.startup.movieku.koin.myAppModule
import com.startup.movieku.utilities.Token
import com.startup.movieku.view.genrelist.GenreVM
import kotlinx.coroutines.runBlocking
import org.junit.*
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class GenreUnitTestingAPi : KoinTest {
   private val vm: GenreVM by inject()

    lateinit var api_key: String
    lateinit var language: String
    lateinit var api_kkey2:String
    lateinit var language2:String

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var observerData: Observer<GenreList>


    @Before
    fun before() {
        api_key = Token.api_key
        language = Token.laguage
        api_kkey2 = "fhgsdjhg"
        language2 ="fdfdf"
        MockitoAnnotations.initMocks(this)
        startKoin { modules(myAppModule) }
    }

    @After
    fun after() {
        stopKoin()
    }

    //positive case
    @Test
    fun apiPositive() = runBlocking {

        vm.dataGenre.observeForever(observerData)
        vm.getGenre(api_key,language)

        Assert.assertNotNull(vm.dataGenre.value)
    }


    //negative case
    @Test
    fun apiNegative() = runBlocking {
        vm.dataGenre.observeForever(observerData)
        vm.getGenre(api_kkey2,language2)
        Assert.assertNotNull(vm.dataGenre.value)
    }
}